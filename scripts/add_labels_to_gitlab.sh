#!/usr/bin/env bash

# /*------------------------------------------------------------------------*\
# |                                                                          |
# | Licenses :     WTFPL (LPRAB) & DSSL apply on this material.              |
# | Méthodes !               Codé @ La Rache                                 |
# |                                                                          |
# +--------------------------------------------------------------------------+
# |                                                                          |
# | pomoterm.sh : A simple script to work with pomodoro habit.               |
# | Copyright (C) 2019-2019 Flyounet - Tous droits réservés.                 |
# |                                                                          |
# | Cette oeuvre est distribuée SANS AUCUNE GARANTIE hormis celle d'être     |
# | distribuée sous les termes de la Licence Demerdez-vous («Demerden Sie    |
# | Sich License») telle que publiée par Flyounet : soit la version 1 de     |
# | cette licence,soit (à  votre gré) toute version ultérieure.              |
# |                                                                          |
# | Vous devriez avoir reçu une copie de la Licence Démerdez-vous avec cette |
# | oeuvre ; si ce n'est pas le cas, consultez :                             |
# | <http://dssl.flyounet.net/licenses/>.                                    |
# |                                                                          |
# +--------------------------------------------------------------------------+
# | Author: Flyounet < dev + add_labels_to_gitlab @@ flyou . net >           |
# +--------------------------------------------------------------------------+
# |                                                                          |
# |                                 . | o | .     ___    |\_/|    ___        |
# |                          HAPPY  . | . | o    /   \_  |` '|  _/   \       |
# |                                 o | o | o   /      \/     \/      \      |
# |                                            /                       \     |
# |                                             \/\/\/\  _____  /\/\/\/      |
# | Under the DSSL and WTFPL (LPRAB) licenses.         \/     \/             |
# |                                                                          |
# \*------------------------------------------------------------------------*/

################################################################################
#  ╦  ╦┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌┬┐┌─┐  ╔═╗┬ ┬┌─┐┌┐┌┌─┐┌─┐     ╔╗ ╔═╗╔═╗╦╔╗╔    #
#  ╚╗╔╝├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐   │ │ │  ║  ├─┤├─┤││││ ┬├┤   ───╠╩╗║╣ ║ ╦║║║║─── #
#   ╚╝ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘   ┴ └─┘  ╚═╝┴ ┴┴ ┴┘└┘└─┘└─┘     ╚═╝╚═╝╚═╝╩╝╚╝    #
################################################################################
#                DON'T CHANGE ANYTHING OUTSIDE THIS BLOCK                      #
################################################################################

tty -s && _TERMINAL_PRINT=1

VERSION='0.0.3'
VERSION_VERBOSE='($Format:%h %d)'

GITLAB_API_TOKEN='FTfZzmyAAXTJ_nZZ6ty6'
JQ_NAME='jq'

################################################################################
#                DON'T CHANGE ANYTHING OUTSIDE THIS BLOCK                      #
################################################################################
#  ╦  ╦┌─┐┬─┐┬┌─┐┌┐ ┬  ┌─┐┌─┐  ┌┬┐┌─┐  ╔═╗┬ ┬┌─┐┌┐┌┌─┐┌─┐     ╔═╗╔╗╔╔╦╗        #
#  ╚╗╔╝├─┤├┬┘│├─┤├┴┐│  ├┤ └─┐   │ │ │  ║  ├─┤├─┤││││ ┬├┤   ───║╣ ║║║ ║║───     #
#   ╚╝ ┴ ┴┴└─┴┴ ┴└─┘┴─┘└─┘└─┘   ┴ └─┘  ╚═╝┴ ┴┴ ┴┘└┘└─┘└─┘     ╚═╝╝╚╝═╩╝        #
################################################################################


################################################################################
#  ╔═╗┬ ┬┌┐┌┌─┐┌┬┐┬┌─┐┌┐┌┌─┐     ╔╗ ╔═╗╔═╗╦╔╗╔                                 #
#  ╠╣ │ │││││   │ ││ ││││└─┐  ───╠╩╗║╣ ║ ╦║║║║───                              #
#  ╚  └─┘┘└┘└─┘ ┴ ┴└─┘┘└┘└─┘     ╚═╝╚═╝╚═╝╩╝╚╝                                 #
################################################################################

# Function to send to screen
_p2s () { printf '%b' "${@}"; }; p2s () { _p2s "${@}\n"; }
_e2s () { _p2s "${@}" >&2; }; e2s () { _e2s "${@}\n"; }
_t2s () { ((_TERMINAL_PRINT)) && _p2s "${@}"; }; t2s () { _t2s "${@}\n"; }
_v2s () { ((_VERBOSE)) && _p2s "${@}"; }; v2s () { _v2s "${@}\n"; }
_die () { _e2s "${@}"; exit 1; }; die () { e2s "${@}"; exit 1; }

# _version	: Print script version (in verbose mode provide also config)
_version () {
	((_VERBOSE)) && local _subversion="${VERSION_VERBOSE}"
	p2s "${0} v${VERSION} ${_subversion:=} © Flyounet(2019-2019)"
}

# _subflags	: Manage long options
_subflags () {
	case "${1,,}" in
		help)		_usage 1; return;;
		verbose)	export _VERBOSE=1; return;;
		version)	export _v=1; return;;
		*)		e2s "Option --${1} invalid !\n"; _usage;; 
	esac
}

# _usage	: Print help to user
_usage () {
	_version

	cat <<EOFUSAGE

usage: pomoterm	[-h] [-v] [-V] 
		[--help] [--verbose] [--version]

EOFUSAGE
	[[ -z "${1:-}" ]] && exit 0
	cat <<EOFHELP

	-T [token]		API Token
	-U [url]		URL to Gitlab repository
	-O			Overwrite exisitng Labels
	-E			Erase existing Labels before proceeding
		-Y
		--yes		Do not ask before erasing Labels
	-L [filename]		Send all commands output to the file 

	-h
        -H
        --help                  Provide help
        -v
        --version               Provide version information
	-V			Verbose print
	--verbose

EOFHELP
	exit 0
}

# _binaryCheck	: Validate needed binaries are present
_binaryCheck () {
	local _cmds="curl sed awk ${JQ_NAME}"
	for _cmd in ${_cmds}; do
		builtin command -v "${_cmd}" &>/dev/null || { local _rc=1; e2s "Command ${_cmd} not found !"; }
	done
	((_rc)) && die "Not able to continue, please install missing commands."
}

# _findIDForProjects	: Find ID of a Project with an un-optimized function :D
_findIDForProjects () {
	local _number_of_page="$( curl -LIks --header "PRIVATE-TOKEN: ${2}" "${1}/api/v4/projects/?per_page=100" | tee -a "${__L}" | awk 'BEGIN { IGNORECASE=1; } /X-Total-Pages:/ {printf $NF;}' | sed -e 's:\r::g' )"
	local _current_page=0
	while (( _current_page != _number_of_page )); do
		echo "Current: ${_current_page} - Total: ${_number_of_page}"
		(( _current_page ++ ))
		while read _id _path _name; do
			[[ ! -z "${3:-}" && "${_path,,}" = "${3,,}" ]] && { _p2s "${_id}"; return; }
		done <<< "$( curl -Lks --header "PRIVATE-TOKEN: ${2}" "${1}/api/v4/projects/?per_page=100&page=${_current_page}" | tee -a "${__L}" | "${JQ_NAME}" -c -r '.[] | [.id, .path_with_namespace, .name] | @tsv' )"
	done
}

# _findIDForProject	: Return ID of a Repository
_findIDForProject () {
	local _cmd="curl -Lks --header 'PRIVATE-TOKEN: ${2}' '${1}/api/v4/projects/${3}'"
	((_L)) && p2s "$(date '+%FT%TZ') --- ${_cmd}" >> "${__L}"
	eval ${_cmd} | tee -a "${__L}" | "${JQ_NAME}" -c -r '[.id] | @tsv'
}

# _findLabelsForProject	: Return list (id, name) of all Labels for a Project
_findLabelsForProject () {
	local _cmd="curl -LIks --header 'PRIVATE-TOKEN: ${2}' '${1}/api/v4/projects/${3}/labels?per_page=100'"
	p2s "\n$(date '+%FT%TZ') --- ${_cmd}" >> "${__L}"
	local _number_of_page="$( eval ${_cmd} | tee -a "${__L}" | awk 'BEGIN { IGNORECASE=1; } /X-Total-Pages:/ {printf $NF;}' | sed -e 's:\r::g' )"
	local _current_page=0
	local _cmd="curl -Lks --header 'PRIVATE-TOKEN: ${2}' '${1}/api/v4/projects/${3}/labels?per_page=100&page=::CURRENTPAGE::'"
	while (( _current_page++ != _number_of_page )); do
		p2s "\n$(date '+%FT%TZ') --- ${_cmd//::CURRENTPAGE::/${_current_page}}" >> "${__L}"
		eval ${_cmd//::CURRENTPAGE::/${_current_page}} | tee -a "${__L}" | "${JQ_NAME}" -c -r '.[] | [.id, .name] | @tsv'
	done
}

# _urlEncode	: urlencode text
_urlEncode () {
	"${JQ_NAME}" -s -R -r @uri
}

# _prepareVariables	: Transform URL into needed variables
#			_name is the Repository Name
#			_space is the Space name (often equivalent to user name)
#			_url is the minimum URI needed to call Gitlab api
_prepareVariables () {
	export _name="${__U##*/}"
	_url="${__U%/*}"
	export _space="${_url##*/}"
	export _url="${_url%/*}"
}

# _eraseInfo	: Print information if user request to erase previous Labels
_eraseInfo () {
	((_E)) && {
		((_y)) || {
			p2s "/!\  Warning ! /!\ "
			p2s "You request to remove all Labels before adding new one."
			p2s "If you think it's a mistake, you have 10 seconds to hit CTRL+C !"
			sleep 10
		}
		p2s '/!\ All existing Labels will be erased. /!\ \n'
	}
}

# _getProjectID	: Export the ID of the project
_getProjectID () {
	t2s "Working on ${_url} for project ${_space}/${_name}"
	_t2s "Looking for project ID ... "
	export _projectID="$( _findIDForProject "${_url}" "${__T}" "$( _p2s "${_space}/${_name}" | _urlEncode )" )"
	[[ -z "${_projectID}" ]] && die "\nUnable to retrieve ID for project ${_space}/${_name}. Aborting..."
	t2s "Found : ${_projectID}"
}

# _getLabelsList	: Retrieve the existing Labels for the project
_getLabelsList () {
	_t2s "Downloading existing Labels list ... "
	export _labelsCurrent="$( _findLabelsForProject "${_url}" "${__T}" "${_projectID}" )"
	_labelsCurrentCount="$([[ -z "${_labelsCurrent}" ]] && _p2s '0'; [[ -z "${_labelsCurrent}" ]] || wc -l <<< "${_labelsCurrent}")"
	t2s "Found : ${_labelsCurrentCount} labels"
}

# _eraseRemoteLabels	: Remove from the Gitlab instance the Labels assocciated with this porject
_eraseRemoteLabels () {
	((_E)) && {
		((_labelsCurrentCount)) || t2s "Nothing to erase."
		((_labelsCurrentCount)) && {
			_t2s "Erasing existing labels ... "
			# Please note the IFS variable contains a tabulation character.
			while IFS='	' read _id _label; do
				((_VERBOSE)) && _t2s "\n - Deleting ${_label} ... "
				local _cmd="curl --request DELETE --header 'PRIVATE-TOKEN: ${2}' '${1}/api/v4/projects/${3}/labels?label_id=${_id}&name=$( _p2s '${_label}' | _urlEncode )'"
				p2s "\n$(date '+%FT%TZ') --- ${_cmd}" >> "${__L}"
				eval ${_cmd} 2>&1 >> "${__L}"
				((_VERBOSE)) && _t2s "Done"
			done <<< "${_labelsCurrent}"
			((_VERBOSE)) && t2s ''
			t2s 'Done'
			export _labelsCurrent=''
		}
	}
}

# _updateRemoteLabels	: create (or update is allowed) remote Labels from local TSV File
_updateRemoteLabels () {
	set +xv
	_LabelsNewFilename="$( cd "${0%/*}"; pwd )/labels.tsv"
	[[ -e "${_LabelsNewFilename}" ]] || die "File ${_LabelsNewFilename} doesn't exist ! Aborting..."
	_labelsNewFilenameCount="$( sed -e '/^\([[:space:]]*#\|$\)/d' "${_LabelsNewFilename}" | sed -n '$=' )"
	_t2s "Creating/Updating ${_labelsNewFilenameCount} new Labels from ${_LabelsNewFilename} ... "
	# Please note the IFS variable contains a tabulation character.
	while IFS='	' read _label _color _description; do
		[[ -z "${_label}" ]] && continue
		grep -qwF "${_label}" <<< "${_labelsCurrent}" && {
			((_VERBOSE)) && _t2s "\n - Label '${_label}' already exist ... "
			((_O)) && {
				local _cmd="curl	-Lks --header 'PRIVATE-TOKEN: ${2}' \
							--request PUT \
							--data-urlencode \"name=${_label}\" \
							--data-urlencode \"color=${_color}\" \
							--data-urlencode \"description=${_description}\" \
							'${1}/api/v4/projects/${3}/labels'"
				p2s "\n$(date '+%FT%TZ') --- ${_cmd}" >> "${__L}"
				eval ${_cmd} 2>&1 >> "${__L}"
			}
			((_VERBOSE)) && {
				((_O)) && _t2s 'Updated.'
				((_O)) || _t2s 'Skipped.'
			}
		} || {
			((_VERBOSE)) && _t2s "\n - Label '${_label}' doesn't exist ... "
			local _cmd="curl	-Lks --header 'PRIVATE-TOKEN: ${2}' \
						--request POST \
						--data-urlencode \"name=${_label}\" \
						--data-urlencode \"color=${_color}\" \
						--data-urlencode \"description=${_description}\" \
						'${1}/api/v4/projects/${3}/labels'"
			p2s "\n$(date '+%FT%TZ') --- ${_cmd}" >> "${__L}"
			eval ${_cmd} 2>&1 >> "${__L}"
			((_VERBOSE)) && _t2s 'Created.'

		}
	done <<< "$( sed -e '/^\([[:space:]]*#\|$\)/d' "${_LabelsNewFilename}" )"
	((_VERBOSE)) && t2s ''
	t2s 'Done'
}

################################################################################
#  ╔═╗┬ ┬┌┐┌┌─┐┌┬┐┬┌─┐┌┐┌┌─┐     ╔═╗╔╗╔╔╦╗                                     #
#  ╠╣ │ │││││   │ ││ ││││└─┐  ───║╣ ║║║ ║║───                                  #
#  ╚  └─┘┘└┘└─┘ ┴ ┴└─┘┘└┘└─┘     ╚═╝╝╚╝═╩╝                                     #
################################################################################


################################################################################
#  ╔╦╗┌─┐┬┌┐┌  ┌─┐┌─┐┬─┐┌┬┐     ╔╗ ╔═╗╔═╗╦╔╗╔                                  #
#  ║║║├─┤││││  ├─┘├─┤├┬┘ │   ───╠╩╗║╣ ║ ╦║║║║───                               #
#  ╩ ╩┴ ┴┴┘└┘  ┴  ┴ ┴┴└─ ┴      ╚═╝╚═╝╚═╝╩╝╚╝                                  #
################################################################################

__T="${GITLAB_API_TOKEN:=}"

while getopts ":-:L:T:U:EhHOvVyY" option; do
	case ${option} in
		E) _E=1;;
		L) _L=1; export __L="${OPTARG}";;
		O) _O=1;;
		T) __T="${OPTARG}";;
		U) __U="${OPTARG}";;
		v) _v=1;;
		V) _VERBOSE=1;;
		[yY]) _y=1;;
		-) _subflags "${OPTARG}";;
		H|h) _usage 1; exit 0;;
		:) e2s "Option -${OPTARG} needs an argument, please read help !\n"; _usage;; 
		\?) e2s "Option -${OPTARG} invalid !\n"; _usage;; 
		*) e2s "Syntax error, please read help.\n"; _usage;;
	esac
done

((_v)) && { _version; exit 0; }

[[ -z "${__L:=}" ]] && export __L='/dev/null'
p2s "\n\n########## $(date "+%FT%TZ") ##########" >> "${__L}"

_binaryCheck

_prepareVariables
_eraseInfo
_getProjectID
_getLabelsList
_eraseRemoteLabels "${_url}" "${__T}" "${_projectID}"
_updateRemoteLabels "${_url}" "${__T}" "${_projectID}"


exit 0

################################################################################
#  ╔╦╗┌─┐┬┌┐┌  ┌─┐┌─┐┬─┐┌┬┐     ╔═╗╔╗╔╔╦╗                                      #
#  ║║║├─┤││││  ├─┘├─┤├┬┘ │   ───║╣ ║║║ ║║───                                   #
#  ╩ ╩┴ ┴┴┘└┘  ┴  ┴ ┴┴└─ ┴      ╚═╝╝╚╝═╩╝                                      #
################################################################################


# +---------------------------------------------------------------------------+
# | $Format: The author of %h was %an, %aD $
# | $Format: The commiter was %cn, %cD $
# +---------------------------------------------------------------------------+

# vim: set cc=80 syntax=sh filetype=sh:

