
When you work in most common Forge, you want to provide support to your projects.

# Labels

They are simple and fits most common practice.

|**Label**|**Color**|**Description**|
|:---|:---:|:---|
|~"Pipe: 1-Reviewed"|`#A8D695`|**Board:** When the issue has been reviewed and ready to be activated.|
|~"Pipe: 2-Next"|`#44AD8E`|**Board:** Issues moved into this Pipe will have to be the next activated issues.|
|~"Pipe: 3-On Going"|`#5CB85C`|**Board:** Issues in this pipe are activated.|
||||
|~"Type: Bug"|`#FF0000`|**Issue:** Related to a non-functionnal issue.|
|~"Type: Enhancement"|`#0033CC`|**Issue:** Request to have a new feature or something enhanced.|
|~"Type: Idea"|`#428BCA`|**Issue** pops up in someone's mind and need to be shared, and why not approve.|
|~"Type: Question/Discussion"|`#8E44AD`|**Issue** is there to share information.|
|~"Type: Security"|`#EE2222`|**Issue** is related to security.|
||||
|~"Priority: 1-Low"|`#D1D100`|**Issue:** exists, and it's already a good thing.|
|~"Priority: 2-Medium"|`#D1D100`|**Issue:** need to be taken into account.|
|~"Priority: 3-High"|`#AD8D43`|**Issue:** is, at least, important for someone.|
|~"Priority: 4-Critical"|`#CC0033`|**Issue:** is deadly important.|
||||
|~"Status: Abandonned"|`#000000`|**Issue/MR** has been closed.|
|~"Status: Blocked"|`#DD3300`|**Issue/MR** is blocked by another **Issue/MR**.|
|~"Status: Completed"|`#00FF00`|**Issue/MR** has been finished/merged.|
|~"Status: Duplicate"|`#EEEEEE`|**Issue/MR** already exists.|
|~"Status: Feedback Needed"|`#FFA500`|**Issue/MR** needs more information to be proceed.|
|~"Status: Help Needed"|`#0033FF`|**Issue/MR**: Not able to do the job, maybe information or work needed.|
|~"Status: In Progress"|`#69D100`|**Issue**: Work in on going (equivalent to the WIP: status in **MR**).|
|~"Status: On Hold"|`#DD3300`|**Issue/MR**: Work on this is halted. |
|~"Status: Refused"|`#222222`|**Issue/MR** will not be managed. Another status should be added.|
|~"Status: Won't Fix"|`#777777`|**Issue** will not be managed. I don't want to.|

Please note :

 * __Pipe: \*__ labels are their for workflow process (mainly Kanban, Board like [Gitlab][], etc.).
 * **MR** (Merge Requests) are also named **PR** (Pull Requests in some tools).

## Licenses

This material is provided under the following licenses :

 * [DSSL][] Demerdern Sie Sich License
 * [LPRAB][] Licence Publique Rien À Branler
 * [WTFPL][] Do What The Fuck You Want To Public License

## Main repository

Please note, the issues referenced in commits where first linked to my
local [Gitea][]. To be able to tackle issues from others, I will now
refer to issues from [this][frama] repository.

[//]: # ( ################## LINKS ################## )

[DSSL]: https://dssl.flyounet.net/ (Demerdern Sie Sich License)
[LPRAB]: http://sam.zoy.org/lprab/ (Licence Publique Rien À Branler)
[WTFPL]: http://www.wtfpl.net/about (Do What The Fuck You Want To Public License)

[Gitea]: https://gitea.io (Git with a cup of tea)
[frama]: https://framagit.org/jcalralc/labels (Provided by Framasoft.org)

[Gitlab]: https://gitlab.com

[//]: # ( vim: set cc=80 tw=85 syntax=markdown: )

