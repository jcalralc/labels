# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- `add_labels_to_gitlab.sh` In case of issue, script is able to provide logs.

## [0.0.2] - 2019-09-22
### Added
- `add_labels_to_gitlab.sh` A script to update your Labels following this guide (#1).
- `labels.tsv` A TSV file with current list of Labels (#1).

### Changed
- `README.md` Updating Priority to remove the letter `P` (#2).

## [0.0.1] - 2019-09-19
### Added
- `README.md` file added.
- `CHANGELOG.md` file added.


[//]: # ( ################################################################### )
[0.0.1]: https://framagit.org/jcalralc/labels/-/tags/0.0.1
[0.0.2]: https://framagit.org/jcalralc/labels/compare/0.0.1...0.0.2

[//]: # ( vim: set cc=80 tw=85 syntax=markdown: )

